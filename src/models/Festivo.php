<?php

namespace Gestec\Dias_Laborales\Src\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Modelo que representa los días festivos.
 * 
 * Un día festivo es aquel día en que no se trabaja por ser fiesta oficial o eclesiástica.
 * 
 * @author Brian Cardona <brian.cardonas@autonoma.edu.co>
 * @version 20180813
 */
class Festivo extends Model {
    protected $table = "festivo";

    const DIAS_ANIO = 365;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fecha', 'descripcion'
    ];

    /**
     * Obtiene los días festivos siguientes desde la fecha actual (hace un año).
     * 
     * Ejemplo: si la fecha actual es 2018-01-01, la función retorna cualquier día festivo superior a la fecha 2017-01-01
     * 
     * @param integer $limite cantidad de días festivos a consultar
     * @return array arreglo con los días festivos
     */
    public static function getFestivos($limite) {
        return Festivo::orderBy('fecha', 'asc')
                        ->where('fecha', '>', Carbon::now()->subDays(self::DIAS_ANIO)->toDateString())
                        ->limit($limite)->pluck('fecha')->toArray();
    }

    /**
     * ¿Se puede eliminar?
     * 
     * Un día festivo puede ser eliminado si la fecha es menor a la fecha actual.
     * 
     * @return boolean true si la fecha puede ser eliminada; false de lo contrario
     */
    public function sePuedeEliminar() {
        $hoy = new Carbon();
        $fechaParaEliminar = new Carbon($this->date);

        return $fechaParaEliminar->greaterThan($hoy);
    }

}
