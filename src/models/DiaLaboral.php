<?php

namespace Gestec\Dias_Laborales\Src\Models;

use Carbon\Carbon;

/**
 * Modelo que representa un día laboral.
 * 
 * Un día laboral es cualquier día entre semana de lunes a viernes que no sea festivo.
 * 
 * @author Brian Cardona <brian.cardonas@autonoma.edu.co>
 * @version 20180813
 */
class DiaLaboral extends Carbon {
    /**
     * Agrega días laborales a una fecha.
     * 
     * Se utiliza una instancia de la clase Carbon para agregar los días;
     * teniendo en cuenta los días festivos.
     * 
     * @param string $date fecha
     * @param integer $days días laborales que serán agregados
     * 
     * @return Carbon\Carbon $fechaCarbon la fecha después de agregar los días
     */
    public static function adicionarDiasLaborales($fecha, $cantidadDias) {
        $fechaCarbon = new DiaLaboral($fecha);
        while ($cantidadDias > 0) {
            $fechaCarbon->addWeekdays(1); 
            if(!$fechaCarbon->esFestivo()) {
                $cantidadDias -= 1;
            }
        }

        return $fechaCarbon;
    }
    
    /**
     * Retorna si la fecha corresponde a un día festivo según la información
     * almacenada en la base de datos.
     * 
     * @return boolean true si es festivo; false de lo contrario
     */
    public function esFestivo() {
        return in_array($this->format('Y-m-d'), 
            Festivo::getFestivos(365)
        );
    }
}
