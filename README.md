# Días laborales

## Instalación

### Utilizando Composer

```sh
composer require gestec/dias-laborales
```

```json
{
    "require": {
        "gestec/dias-laborales": "^2.0.0"
    }
}
```

## Configuración

Antes de poder utilizar la libreria, se debe crear la `migración` donde quedarán registrados los días festivos y/o no laborales. Se utiliza artisan para esta labor.

1. Crear la migración
```sh
php artisan make:migration crearTablaFestivo
```

2. Agregar el esquema a la migración en public function up()
```php
Schema::create('festivo', function (Blueprint $table) {
    $table->increments('id');
    $table->date('fecha')->unique()->comment('Fecha del festivo o día no laboral');
    $table->string('descripcion', 100)->comment('Descripción');
    $table->timestamps();
});
```

3. Agregar en public function down()
```php
Schema::dropIfExists('festivo');
```

4. Correr las migraciones
```sh
php artisan migrate
```

## Uso

```php
use Gestec\DiasLaborales\Models\DiaLaboral;
use Gestec\DiasLaborales\Models\Festivo;

//Agregar 15 días laborales a una fecha
$fecha = Carbon::createFromDate(2020, 7, 15);
$cantidadDias = 15;
$nuevaFecha = DiaLaboral::adicionarDiasLaborales($fecha, $cantidadDias);

//Obtener 100 días festivos
$diasFestivos = Festivo::getFestivos(100);
```

## Información

<a href="https://www.autonoma.edu.co/" target="_blank"> Universidad Autónoma de Manizales </a>
