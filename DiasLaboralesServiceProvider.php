<?php 

namespace Gestec\Dias_Laborales;

use Illuminate\Support\ServiceProvider;

class DiasLaboralesServiceProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register the application services.
     * 
     * @return void
     */
    public function register() {
        //
    }

}